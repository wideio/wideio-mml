# -*- coding: utf-8 -*-


def html_indent(buf):
    from bs4 import BeautifulSoup
    try:
        return BeautifulSoup(buf, 'html.parser').prettify(formatter=None)  # .decode('utf8')
    except:
        x = BeautifulSoup(buf, "html5lib")
        #x=list(list(x.children)[0])[0]
        return x.prettify(formatter=None)  # .decode('utf8')

from transductor import Transductor


class OutputBuf:
    monospaced = False
    nlstrip = True
    prettify_html = False
    transduction = None

    def __init__(self):
        self.buf = u""

    def append(self, x):
        self.buf += x

    def get(self):
        from mml_errors import mmlwarning
        buf = self.buf
        if (self.nlstrip):
            bufl = buf.split(u"\n")
            i = 0
            if (bufl != []):
                while ((bufl != [])and (bufl[0].strip() == u"")):
                    bufl.pop(0)
                while ((bufl != [])and(bufl[-1].strip() == u"")):
                    bufl.pop(-1)
                buf = (u"\n").join(bufl) + ("\n" if buf[-1] == "\n" else "")
        if (self.prettify_html):
            try:
                buf = html_indent(buf)
            except Exception as e:
                mmlwarning("Failed to prettify HTML -- Please check HTML validity..." + str(e))
        if (self.transduction):
            buf = Transductor(self.transduction).process(buf, 256)
        if (self.monospaced):
            import re
            buf = re.subn(u"([ \t\n]+)", u" ", buf)[0]
        return buf


class NullOutputBuf:
    def __init__(self):
        pass

    def append(self, x):
        pass

    def get(self):
        return u""
