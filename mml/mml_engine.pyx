import re
import sys
import os
import inspect
from mml.mml_errors import *
from mml.mml_output import OutputBuf, NullOutputBuf

cdef extern from "wchar.h":
    ctypedef struct wchar_t:
        pass
    int wprintf (wchar_t * format, ...)


cdef extern from "Python.h":
    ctypedef unsigned long size_t
    object PyBuffer_FromMemory( void *ptr, int size)
    object PyBuffer_FromReadWriteMemory( void *ptr, int size)
    object PyString_FromStringAndSize(char *s, int len)
    void* PyMem_Malloc( size_t n)
    void PyMem_Free( void *p)

cdef extern from "unicodeobject.h":
    object PyUnicode_FromWideChar(wchar_t *w, size_t size )


    Py_ssize_t PyUnicode_AsWideChar( unicode _unicode, wchar_t *w,  size_t size  )


    ctypedef struct PyUnicodeObject:
        size_t length
        unsigned short *str
        long hash


cdef extern from "ctype.h":
    int isalpha(char c)
    int isalnum(char c)
    int isdigit(char c)
    int isspace(char c)


cdef extern from "wctype.h":
    int iswalpha(wchar_t c)
    int iswalnum(wchar_t c)
    int iswdigit(wchar_t c)
    int iswspace(wchar_t c)



class MacroBlock(object):
    def __init__(self):
        self.bblocktext=None
        self.btext=None
        self.etext=None
        self.ietext=None
        self.text=None
        self.relative=False
        self.bpass=0
        self.epass=0
        self.startblk=False
        self.protected=False
    def write_macro_header(self,context,dpass,drelative):
        header=u"%"
        if (self.protected):
            header+=u"#"
        if (not drelative) and (self.relative):
            header+=u"+"
            dpass=0
        header+=str(max(0,self.bpass-dpass))
        if (self.epass-dpass>0):
            if (self.epass!=self.bpass):
                header="-"+str(self.epass-dpass)
        header+=u"["
        context.output(header)



cdef class MmlBlockParser:
    cdef object bufname
    cdef int lineno
    cdef object context
    def __init__(self,bufname="**unamed buffer**",context=None):
        self.bufname=bufname
        self.lineno=1
        self.context=context
    def get_bufname(self):
        return self.bufname
    def get_lineno(self):
        return self.lineno
    def _go_to_end_of_zone(self, object strx, int pos):
        cdef int l
        cdef int o
        cdef int ipos
        cdef long cc=0
        cdef wchar_t* strb=<wchar_t *>PyMem_Malloc(4*len(strx)+4)
        ipos=pos
        PyUnicode_AsWideChar(strx,strb,len(strx))
        l=0
        o=0
        lenstrb=len(strx)
        #print strx, len(strx), pos
        if (pos>=lenstrb):
            raise IndexError
        cc=<long>strb[pos]; pos+=1
        #wprintf(strb)
        while ((l!=-1)):
            if (cc==10): #\n
                self.lineno+=1
            if (cc==92): #\\
                if (pos>=lenstrb):
                    raise IndexError
                cc=<long>strb[pos]; pos+=1
                o+=1
                if (cc==10):#'\n'
                    self.lineno+=1
            else:
                if cc==34: #'"'
                    if (pos>=lenstrb):
                        raise IndexError
                    cc=<long>strb[pos]; pos+=1
                    while (cc!=34): # '"'
                        if (cc==92): #\\
                            if (pos>=lenstrb):
                                raise IndexError
                            cc=<long>strb[pos]; pos+=1
                            o+=1
                        if (cc==10): #\n
                            self.lineno+=1
                        try:
                            if (pos>=lenstrb):
                                raise IndexError
                            cc=<long>strb[pos]; pos+=1
                        except IndexError:
                            mmlerror(u"Unfinished string in block beginning by: "+(strx[ipos:(ipos+20)]) +"...",self.context)
                            #raise Exception, "Unfinished string"
            if (cc==91): #[
                l+=1
            if (cc==93): #]
                l-=1
            o+=1
            try:
                if (pos>=lenstrb):
                    raise IndexError
                cc=<long>strb[pos]; pos+=1
            except IndexError:
                if (l==-1):
                    return pos-1
                mmlerror("Unclosed bracket l=%d (last char codec=0x%x), block beginning by : %s"%(l,cc,strx[ipos:(ipos+20)]),self.context)
                #raise "Exception", "Unclosed bracket"
        if (l!=-1):
            mmlerror("Unclosed bracket  =%d"%(l,),self.context)
            #raise Exception, "Unclosed bracket"
        PyMem_Free(<void *>strb)
        return pos-2


    def try_get_macroblock(self, object strb,int pos):
        """
           tries to read one expression of the forn %()[macro ...], possibly including other macrocalls
        """
        cdef int state
        cdef int bnum
        cdef int epos
        cc=strb[pos]; pos+=1
        ## is that a macro block ?
        if (cc!=u"%"):
            return None
        cc=strb[pos]; pos+=1
        if (cc==u"%"):
            return None
        else:
            ## hmmm it seems to well let's try to read it completely
            ## we read hte macroblock header first
            #print "MBX"
            mb=MacroBlock()
            state=0
            cpass_buf=u""
            #print (dir(it))
            posb=pos
            mb.bblocktext=pos
            while True:
                #print state
                if state==0:
                            # read next character until %
                    if (cc.isnumeric() ):
                        state=1
                        bnum=pos-1
                        cpass_buf+=cc
                    elif (cc==u'+'):
                        mb.relative=True
                        cc=strb[pos]; pos+=1
                    elif (cc==u'#'):
                        mb.protected=True
                        cc=strb[pos]; pos+=1
                    elif (cc ==u'!'):
                        mb.lastpass=65536
                        state=4
                        cc=strb[pos]; pos+=1
                    elif (cc==u'*'):
                        cc=strb[pos]; pos+=1
                        mb.startblock=True
                    elif (cc==u'-'):
                        state=3
                        cc=strb[pos]; pos+=1
                    elif (cc==u'['):
                        state=5
                        #cc=strb[pos]; pos+=1
#                    elif (cc==u'$'):
#                        #cc=strb[pos]; pos+=1
#                        text_beg=pos
#                        state=6
                    else:
                        return None
                while (state==1):
                    if (cc.isnumeric()):
                        state=1
                        cc=strb[pos]; pos+=1
                    else:
                        if (pos!=bnum):
                            #print "num?", strb[bnum:pos]
                            mb.bpass=int(strb[bnum:(pos-1)])
                            mb.epass=mb.bpass
                        state=3
                if state==2: ## a minus is to be read
                    if (cc==u'-'):
                        state=3
                        cc=strb[pos]; pos+=1
                    elif (cc==u'$'):
                        state=6
                    elif (cc==u'['):
                        state=5
                    else:
                        return None
                while (state==3):
                    bnum=pos
                    if (cc.isnumeric()):
                        state=3
                        cc=strb[pos]; pos+=1
                    else:
                        if (pos!=bnum):
                            mb.epass=int(strb[bnum:pos])
                        state=4
                if state==4: ## min and max have been read the block has to begin else error
                    if (cc==u'$'):
                        state=6
                    elif (cc==u'['):
                        state=5
                    else:
                        return None
                if state==5:
                    ## until close
                    mb.btext=pos
                    mb.etext=self._go_to_end_of_zone(strb,pos)
                    if (strb[mb.etext-1]==u"/") and (mb.etext!=(mb.btext+1)):
                        mb.ietext=mb.etext-1
                        return mb#, strb[mb.btext:(mb.etext-1)]
                    else:
                        mb.ietext=mb.etext
                        return mb#, strb[mb.btext:mb.etext]
                if state==6:
                    ## read until end of word
                    mb.btext=pos
                    mb.etext=self._go_to_end_of_zone(strb, pos)
                    if (strb[mb.etext-1]==u"/") and (mb.etext!=(mb.btext+1)):
                        mb.ietext=mb.etext-1
                        return mb#, strb[mb.btext:(mb.etext-1)]
                    else:
                        mb.ietext=mb.etext
                        return mb#, strb[mb.btext:mb.etext]
    def parse_mb(self,buf):
        buf.split(u' ')
        return (buf[0], self.read_args(buf[:1]) )
    def expand_macros(self,strb,int pos, context):
        cdef int bpos
        lenstrb=len(strb)
        bpos=pos
        try:
            while True:
                if (pos>=lenstrb):
                    context.output(strb[bpos:pos])
                    break
                cc=strb[pos]; pos+=1
                if (cc==u'\n'):
                    self.lineno+=1
                    #context.output(cc)
                elif (cc==u'%'):
                    epos=pos-1
                    try:
                        mb=self.try_get_macroblock(strb,pos-1)
                    except IndexError:
                        mmlerror("Macro block probably unfinished :"+str((pos,strb[(pos-1):(pos+255)])),context)
#                    if (mb) and ((not context.protect_until) or ( (mb.protected) and (strb[mb.btext:mb.ietext].split(u' ')[0] in context.protect_until) and ((mb.bpass==0) and ((not mb.relative) or  (context.with_relative))))):
                    if (mb) and ((not context.protect_until) or ( (mb.protected) and (strb[mb.btext:mb.ietext].split(u' ')[0] in context.protect_until) and (((not mb.relative) or  (context.with_relative))))):

                        context.output(strb[bpos:epos])
                        if ((mb.bpass==0) and ((not mb.relative) or  (context.with_relative))):
                            context.push_output(OutputBuf())
                            self.expand_macros(strb[mb.btext:mb.ietext],0,context)
                            rmb=context.pop_output().buf
                            context.interpret_macro(rmb,mb)
                        if ((mb.epass!=0) or (mb.relative and not context.with_relative)):
                            mb.write_macro_header(context,1,context.with_relative)
                            self.expand_macros(strb[mb.btext:mb.ietext],0,context)
                            context.output(u']')
                        #context.output(u"MACROB:["+rmb+"]")
                        #print "etext,",mb.btext,mb.etext
                        pos+=mb.etext-mb.bblocktext+2
                        bpos=pos
                    #else:
                        #context.output(cc)
                #else:
                #    context.output(cc)
        except IndexError:
            mmlwarning("IndexError during expand_macros (EOF)",context)
            pass


def parse_arguments_for_kw(cls,xargs,context, fargs, fallargs):
    cdef int pos
    #cdef wchar_t * args
    pos=0
    d=[]
    if fallargs==None:
        fallargs="extraargs"
    k=None
    v=None
    cno=0
    bpos=None
    largs=len(xargs)
    #if (not xargs):
    #  mmlerror("No arguments",context)
    #sys.stderr.write(xargs)
    #args=<wchar_t*>xargs
    cdef wchar_t* args=<wchar_t *>PyMem_Malloc(4*len(xargs)+4)
    PyUnicode_AsWideChar(xargs,args,len(xargs))
    try:
        while True:
            is_id=False
            if ( pos >= largs ):
                raise IndexError
            while iswspace(args[pos]):
                pos+=1
                if ( pos >= largs ):
                    raise IndexError
            if (iswalpha(args[pos]) or (<long>args[pos]) in [ 38, 95 , 46 ]): # &_.
                # id
                if ( (<long>args[pos])== 38 ): # &
                    is_id=True
                    pos+=1
                    if ( pos >= largs ):
                        raise IndexError
                bpos=pos
                while ( iswalnum(args[pos]) or (<long>args[pos]) in [95,46] ): #_.
                    pos+=1
                    if ( pos >= largs ):
                        raise IndexError
                epos=pos
                if (k==None) and  ((<long>args[pos])==61): # =
                    assert(not is_id)
                    k=xargs[bpos:epos]
                else :
                    if (is_id):
                        v=context.get_var(xargs[bpos:epos])
                        is_id=False
                    else:
                        v=xargs[bpos:epos]
                    if (k==None):
                        try:
                            k=fargs.pop(0)
                        except:
                            k=fallargs+str(cno)
                            cno+=1
#                        sys.stderr.write(str((k,v))+"1\n")
                    d.append((k,v))
                    k=None
                    v=None
                    bpos=None
            elif (<long>args[pos])==34 :#"
                pos+=1
                bpos=pos
                if ( pos >= largs ):
                    raise IndexError
                while ((<long>args[pos])!=34):
                    if ((<long>args[pos])==92): #\\
                        pos+=2
                    else:
                        pos+=1
                    if ( pos >= largs ):
                        raise IndexError
                v=xargs[bpos:pos]
                if (k==None):
                    try:
                        k=fargs.pop(0)
                    except:
                        k=fallargs+str(cno)
                        cno+=1
#                sys.stderr.write(str((k,v))+"2\n")
                d.append((k,v))
                k=None
                bpos=None
                v=None
                pos+=1
            elif (<long>args[pos])==39: #'
                pos+=1
                bpos=pos
                if ( pos >= largs ):
                    raise IndexError
                while ((<long>args[pos])!=39): #'
                    if ((<long>args[pos])==92):
                        pos+=2
                    else:
                        pos+=1
                    if ( pos >= largs ):
                        raise IndexError
                if ( pos >= largs ):
                    raise IndexError
                v=xargs[bpos:pos]
                if (k==None):
                    try:
                        k=fargs.pop(0)
                    except:
                        k=fallargs+str(cno)
                        cno+=1
#                sys.stderr.write(str((k,v))+"3\n")
                d.append((k,v))
                k=None
                v=None
                bpos=None
                pos+=1
            elif iswdigit(args[pos]):
                bpos=pos
                while ( iswdigit(args[pos]) or (<long>args[pos]) in [ 46, 101] ):
                    pos+=1
                    if ( pos >= largs ):
                        raise IndexError
                epos=pos
                if ( pos >= largs ):
                    raise IndexError
                v=xargs[bpos:epos]
                if (k==None):
                    try:
                        k=fargs.pop(0)
                    except:
                        k=fallargs+str(cno)
                        cno+=1
#                sys.stderr.write(str((k,v))+"4\n")
                d.append((k,v))
                k=None
                v=None
                bpos=None
            elif (<long>args[pos])==61:
                assert(k!=None)
                pos+=1
            else:
                mmlerror(("Syntax error while parsing arguments" +str(( xargs,<long>args[pos],d))),context)
                    #raise Exception, ("Syntax error while parsing arguments", <long>args[pos],d)
            if ( pos >= largs ):
                raise IndexError
    except IndexError:
        #print "ie"
        if (bpos!=None):
            epos=pos
            if (epos>bpos):
                if (is_id):
                    v=context.get_var(xargs[bpos:epos])
                    is_id=False
                else:
                    v=xargs[bpos:epos+1]
                if (v):
                    if k==None:
                        try:
                            k=fargs.pop(0)
                        except:
                            k=fallargs+str(cno)
                            cno+=1
                    try:
                        v=float(v)
                    except:
                        pass
#                    sys.stderr.write(str((k,v))+"5\n")
                    d.append((k,v))
                    k=None
                    v=None
    PyMem_Free(args)
    return d
