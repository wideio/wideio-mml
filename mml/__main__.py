#!/usr/bin/env python
# -*- coding: utf-8 -*-

###
###

from mml import mml_errors
from mml.mml_core import *
from mml.mml_keywords import *

stdcommands = dict([(v[4:], eval(v)) for v in filter(lambda x:x[:4] == "Cmd_", globals())])


class MmlContext():
    show_mml_trace_on_warning = True
    show_callstack_on_warning = True
    display_warnings = True
    verbose = False

    def __init__(self):
        #self.macros=stdcommands  #{}
        self.symbols = dict(map(lambda i: (i[0], [unicode(i[1])]), os.environ.items()))
        for i in stdcommands.items():
            if i[0] in self.symbols:
                self.symbols[i[0]].append(i[1])
            else:
                self.symbols[i[0]] = [i[1]]
        self.opened_env = []
        self.opened_def_env = []
        self.divert_stack = []
        self.callstack = []
        self.outputs = [OutputBuf()]
        self.readers = []
        self.with_relative = False
        self.protect_until = None
        self.include_pathes = ["."]
        self.minpass = 1
        self.maxpass = 1
        self.done_subst = 0

    def push_output(self, out):
        self.outputs.append(out)

    def pop_output(self):
        return self.outputs.pop(-1)

    def output(self, outtxt):
        if (not isinstance(outtxt, unicode)):
            if (type(outtxt) not in [float]):
                mmlwarning(u"passing non unicode (" + str(type(outtxt)) + "|" + unicode(outtxt) + ")", self)
            outtxt = unicode(outtxt)
        if (len(self.outputs) == 0):
            return
        #assert(len(self.outputs)>=1)
        self.outputs[-1].append(outtxt)

    def push_call(self, call):
        self.callstack.append((call, self.readers[-1][0].get_bufname(), self.readers[-1][0].get_lineno()))

    def pop_call(self):
        return self.callstack.pop(-1)[0]

    def _pop_call(self):
        return self.callstack.pop(-1)

    def push_interpret(self, text, name="** unnamed buffer **"):
        mb = MmlBlockParser(name, self)
        self.readers.append((mb, text))
        mb.expand_macros(text, 0, self)
        self.readers.pop(-1)

    def push_var(self, k, v):
        if k in self.symbols:
            self.symbols[k].append(v)
        else:
            self.symbols[k] = [v]

    def append_var(self, k, v):
        if k in self.symbols and len(self.symbols[k]):
            self.symbols[k][-1] += v
        else:
            self.symbols[k] = [v]

    def pop_var(self, k):
        if k in self.symbols:
            r = self.symbols[k].pop(-1)
            if (len(self.symbols[k]) == 0):
                del self.symbols[k]
            return r
        else:
            mmlwarning(u"Trying to pop non existing variable : " + k, self)
            return u""

    def set_var(self, k, v):
        if (k in self.symbols):
            self.symbols[k][-1] = v
        else:
            self.symbols[k] = [v]

    def reset_var(self, k):
        if (k in self.symbols):
            del self.symbols[k]

    def open_env(self, env):
        self.opened_env.insert(0, env)

    def close_env(self, env):
        for x in range(len(self.opened_env)):
            if self.opened_env[x] == env:
                self.opened_env.pop(x)
                break

    def get_var(self, k, warning_if_not_found=warning_if_not_found, reinterpret_on_expand=reinterpret_on_expand):
        if k in self.symbols:
            if (reinterpret_on_expand):
                self.push_output(OutputBuf())
                tbuf = self.symbols[k][-1]
                if (not isinstance(tbuf, unicode)):
                    if (type(tbuf) not in [float]):
                        mmlwarning("deprecated conversion from " + str(type(tbuf)) + " to unicode", self)
                    tbuf = unicode(tbuf)
                self.push_interpret(tbuf, "** reinterpret **" + k)
                bo = self.pop_output()
                return bo.buf
            else:
                return self.symbols[k][-1]
        else:
            if (warning_if_not_found):
                mmlwarning(u"trying to get non existing variable : " + k, self)
            return u""

    def interpret_macro(self, rmb, mb):
        try:
            bufsplit = rmb.index(u" ")
        except:
            bufsplit = len(rmb)
        args0 = rmb[:bufsplit]
        #print rmb
        try:
            try:
                if args0[0] == u"/":
                    xargs0 = u"end_" + args0[1:]
                else:
                    xargs0 = args0
                #print "looking for",xargs0,"|", rmb, type(args0)
                ## OLD ENV SEARCH
                #for i in range(len(self.opened_env)+1):
                #    mname='.'.join(self.opened_env[i:]+[xargs0])
                #    if self.macros.has_key(mname):
                #        break

                ## NEW ENV SEARCH
                mname = None
                #sys.stderr.write( "env "+str(self.opened_env)+","+xargs0+"\n")
                for ie in range(len(self.opened_env)):
                    #ls=self.opened_env[-(1+ie)].split('.')
                    ls = self.opened_env[ie].split('.')
                    for ik in range(len(ls)):
                        tname = ".".join(ls[ik:] + [xargs0])
                        #sys.stderr.write( "trying "+str(ik)+" "+str(tname)+"\n")
                        if tname in self.symbols:
                            mname = tname
                            break
                    if mname:
                        break
                if not mname:
                    mname = xargs0
                if not hasattr(self.symbols[mname][-1], "call"):
                    raise KeyError
                #sys.stderr.write( "trying to call " + str(xargs0)+"->"+str(mname)+","+str(self.symbols.has_key(mname))+"\n")
                #sys.stderr.write(unicode(self.symbols[mname][-1].call))
                try:
                    md = self.symbols[mname][-1].call(rmb[bufsplit:], self, mb)
                    #sys.stderr.write( "call done " + str(xargs0)+"->"+str(mname)+"\n")
                except TypeError:
                    mmlerror(u"syntax/macro error calling macro " + mname + " " + repr(xargs0) + " " + repr(mb), self)
            except KeyError:
                try:
                    self.output(self.symbols[args0][-1])
                except (KeyError, IndexError):
                    if (warning_if_not_found):
                        mmlwarning(u"no macro or variable with that name '" + args0 + "'", self)
                        # in  " + str(self.macros.keys()))
                    pass
        except Exception as e:
            mmlerror("Unknown exception while parsing macro:" + str((args0, e)), self)

    def einclude(self, filename, exception_on_not_found=True):
        xp = False
        for p in self.include_pathes:
            try:
                os.stat(p + '/' + filename)
                xp = True
            except OSError:
                pass
            if xp:
                fbuf = file(p + '/' + filename).read().decode('utf8')
                self.push_interpret(fbuf, p + '/' + filename)
                return
        if (exception_on_not_found):
            sys.stderr.write("MML include not found " + filename)
            raise Exception("MML include not found" + filename)
        else:
            sys.stderr.write("MML include not found " + filename)


##############################################################################
##############################################################################


def print_help_and_exit():
    print "MML / Aka MetaMacroProvessor v1.0 -- Copyrights Bertrand NOUVEL (WIDE IO LTD)"
    print "Distributed in LGPL \n"
    print """
 Options :
     -u/--use <namespace> : open namespacea
     -i/--include <file>  : includes a file (with output diverted to NULL)!
     -I/--addincludedir <dir> : append a directory to the options
     -m/--mode  (raw/rawc/html/htmlc)    : mode
     --Wm                     : toggle display of mml trace on warning
     --Wc                     : toggle display of mml callstack on warning
     -h/--help                : display this message
    """
    sys.exit(-1)


def mml_main():
    import getopt
    import itertools
    go = getopt.getopt(sys.argv[1:], 'vhi:I:u:m:', ['verbose', 'Wm', 'Wc', 'help', 'use=', 'addincludedir=', 'include=', 'mode='])
    ctx = MmlContext()
    ctx.outputs = []  # OutputBuf()]
    prettify_html = True
    #html_transductor={'a':[(u"{%",None,'b') ] , 'b':[(u"%}",None,'a'),(u'\n',u" "),(u"'",u'"')]  }
    html_transductor = {'a': [(u"{%", None, 'b')], 'b': [(u"%}", None, 'a'), (u'\n', u" ")]}
    htmlc_transductor = {'a': [(u"{%", None, 'b'), (u"([ \t\n]+)", u" ")], 'b': [(u"%}", None, 'a'), (u'\n', u" ")]}
    html2_transductor = {'a': [(u"{%", None, 'b'), (u"<", None, 'aa'), (u"([ \t\n]+)", u" ")],
                         'aa': [
        (u"(img|br|input|embed|base|col|command|hr|area|meta|link|param|track|source|wbr)(([^>]*[^/])?)([^/]?)>", lambda g:(g.group(1) + g.group(2)).strip() + "/>", 'a'),
        (u"script([^>]*)>", lambda x:x.group(0) + u"/*<![CDATA[*/", 'c'),
        (u"style([^>]*)>", lambda x:x.group(0) + u"/*<![CDATA[*/", 'cx'),
        (u"pre([^>]*)>", None, 'p'),
        (u".", None, 'a')],
        'c': [(u"</script", lambda x:u"/*]]>*/" + x.group(0), 'a'), (u"//([^\n]+)", ""), (u"/\\*", "", 'd'), (u"'", None, 'e'), (u"\"", None, 'f'),
              (u"([ \t\n]+)", u" ")],
        'cx': [(u"</style([^>]*)>", lambda x:u"/*]]>*/" + x.group(0), 'a'), (u"//([^\n]+)", ""), (u"/\\*", "", 'dx'), (u"'", None, 'ex'), (u"\"", None, 'fx'),
               (u"([ \t\n]+)", u" ")],
        'd': [(u"\\*/", "", 'c'), (".", "")],
        'e': [(u"'", None, 'c')],
        'f': [(u'"', None, 'c')],
        'dx': [(u"\\*/", "", 'cx'), (".", "")],
        'ex': [(u"'", None, 'cx')],
        'fx': [(u'"', None, 'cx')],
        'b': [(u"%}", None, 'a'), (u'\n', u" ")],
        'p': [(u"</pre([^>]*)>", None, 'a'), (u"//([^\n]+)", "")]
    }
    js_transductor = {'a': [(u"//([^\n]+)", ""), (u"/\\*~", None, 'dx'), (u"/\\*", "", 'd'), (u"'", None, 'e'), (u"\"", None, 'f'),
                            (u"([ \t\n]+)", u" ")],
                      'd': [(u"\\*/", "", 'a'), (".", "")],
                      'dx': [(u"\\*/", None, 'a')],
                      'e': [(u"'", None, 'a')],
                      'f': [(u'"', None, 'a')],
                      }

    transductor = html_transductor
    for o in go[0]:
        if (len(o)):
            if ((o[0] == '-u') or (o[0] == '--use')):
                ctx.opened_env.append(o[1])
            elif (o[0] == '-i' or o[0] == '--include'):
                #sys.stderr.write("including "+o[1]+"\n")
                ctx.einclude(o[1])
            elif (o[0] == '-I' or o[0] == '--addincludedir'):
                ctx.include_pathes.append(o[1])
            elif (o[0] == '-h') or (o[0] == '--help'):
                print_help_and_exit()
            elif (o[0] == '-v') or (o[0] == '--verbose'):
                ctx.verbose = True
            elif (o[0] == "--Wm"):
                ctx.show_mml_trace_on_warning = not ctx.show_mml_trace_on_warning
            elif (o[0] == "--Wc"):
                ctx.show_callstack_on_warning = not ctx.show_callstack_on_warning
            elif (o[0] == '-m') or (o[0] == "--mode"):
                if (o[1] == "raw"):
                    prettify_html = False
                    transductor = {}
                elif (o[1] == "rawc"):
                    prettify_html = False
                    transductor = html2_transductor
                elif (o[1] == "js"):
                    prettify_html = False
                    transductor = js_transductor
                elif (o[1] == "html"):
                    prettify_html = True
                    transductor = html_transductor
                elif (o[1] == "htmlc"):
                    prettify_html = True
                    transductor = htmlc_transductor
    mmlassert(len(ctx.outputs) == 0, ctx)
    mmlassert(ctx.callstack == [], ctx)
    fnames = go[1]
    ob = OutputBuf()
    ob.prettify_html = prettify_html
    ob.transduction = transductor
    ctx.outputs = [ob]
    filelist = itertools.imap(lambda x: (file(x), x), fnames)
    if (not len(fnames)):
        filelist = [(sys.stdin, "<stdin>")]
        #buf=f.read().decode('utf8')

    try:
        for f in filelist:
            if ctx.verbose:
                sys.stderr.write("reading " + f[1] + "\n")
            for passno in range(1):
                ctx.push_interpret(f[0].read().decode('utf8'), f[1])
    except Exception as e:
        print "Error :", e
        if (hasattr(sys, "last_traceback")):
            traceback.print_tb(sys.last_traceback)
        else:
            traceback.print_tb(sys.exc_info()[2])
        mmlerror("Critical MML error (see traceback before)", ctx)
    buf = ctx.outputs[0].get()
    sys.stdout.write(buf.encode('utf8'))
    if not(ctx.callstack == []):
        mmlerror("MML finishing but call-stack is not empty", ctx)
    if not(len(ctx.outputs) == 1):
        sys.stderr.write("Remaining outputs:\n")
        for x in range(len(ctx.outputs)):
            sys.stderr.write(" - \"" + ctx.outputs[x].get()[:40] + "...\"\n")
        mmlerror("MML finishing but output-stack is not back to 1 normal output,  remaining outputs=: " + str(len(ctx.outputs)), ctx)


def mmli(s):
    ctx = MmlContext()
    ctx.push_interpret(unicode(s), "<mmli>")
    return ctx.outputs[0].get()

if __name__ == "__main__":
    mml_main()
