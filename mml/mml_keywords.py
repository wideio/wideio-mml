# -*- coding: utf-8 -*-
import sys
import textwrap
from functools import reduce

from mml_core import *
from transductor import CachedTransductor, CompiledTransductor, Transductor


class Cmd_def(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        argsl = filter(lambda s: len(s) != 0, args.split(u' '))
        defid = argsl[0]
        if (not((defid not in context.symbols) or (not hasattr(context.symbols[defid][-1], 'is_overridable')) or context.symbols[defid][-1].is_overridable)):
            mmlerror("You cannot override a this macro +'" + defid + "'", context)
        cname = u'.'.join(context.opened_def_env + [defid])
        no = OutputBuf()
        context.push_output(no)
        declargs = cls.get_declared_arguments(u' '.join(argsl[1:]), context)
        context.push_call(("DEF", cname, declargs, no))


class Cmd_end_def(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        #argsl=filter(lambda s:len(s)!=0,args.split(u' '))
        #assert((not context.macros.has_key(defid)) or  context.macros[defif].is_overridable)
        topstack = context.pop_call()
        if (not(topstack[0] == "DEF")):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "DEF"), context)
        #sys.stderr.write("/def" +str(context.callstack))
        macrotext = context.pop_output().buf
        targs = map(lambda x: (x[0][0] == "*") and x[1] or x[0], topstack[2])
        tdefault_args = dict(filter(lambda x: x[0][0] != "*", topstack[2]))

        class UserMacro(MmlMacro):
            name = topstack[1]
            args = targs
            argsdefaults = tdefault_args
            buf = macrotext
        #sys.stderr.write("ALMOST DONE\n" +unicode(topstack[1]))
        context.push_var(topstack[1], UserMacro)
        #sys.stderr.write( "added " + str( topstack[1] ) +"\n")


class Cmd_def_env(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        argsl = filter(lambda s: len(s) != 0, args.split(u' '))
        defid = argsl[0]
        if (not((defid not in context.symbols) or (not hasattr(context.symbols[defid][-1], 'is_overridable')) or context.symbols[defid][-1].is_overridable)):
            mmlerror("You cannot override a this marcro +'" + defid + "'", context)
        cname = u'.'.join(context.opened_def_env + [defid])
        #cname=defid
        no = OutputBuf()
        context.push_output(no)
        declargs = cls.get_declared_arguments(u' '.join(argsl[1:]), context)
        context.opened_def_env.append(defid)
        #sys.stderr.write(str(declargs)+"\n")
        context.push_call(("DEF_ENV", cname, declargs, no))


class Cmd_end_def_env(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        topstack = context.pop_call()
        if (not (topstack[0] == "DEF_ENV")):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "DEF_ENV"), context)
        context.opened_def_env.pop(-1)
        macrotext = context.pop_output().buf
        #print topstack
        targs = map(lambda x: (x[0][0] == "*") and x[1] or x[0], topstack[2])
        tdefault_args = dict(filter(lambda x: x[0][0] != "*", topstack[2]))
        rname = topstack[1].split(u".")[-1]
        endname = u".".join(topstack[1].split(u".")[:-1] + [
            rname, ("end_" + rname)])

        class UserMacroB(MmlMacroEB):
            name = topstack[1]
            tag = topstack[1]
            protect_until = ["end_" + rname, "/" + rname]  # TODO : in all envs
            args = targs
            argsdefaults = tdefault_args

        class UserMacroE(MmlMacroEE):
            name = endname
            tag = topstack[1]
            buf = macrotext
        context.push_var(topstack[1], UserMacroB)
        context.push_var(endname, UserMacroE)
        #sys.stderr.write( "added " + str( topstack[1] ) +"\n")


class Cmd_set(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        for k in filter(lambda x: x[:2] != "__", kwargs.keys()):
            if (k[0] != "*"):
                context.set_var(k, kwargs[k])
            else:
                mmlwarning("invalid syntax for set/ all arguments must be in the form 'key=value'")


class Cmd_push(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        for k in filter(lambda x: x[:2] != "__", kwargs.keys()):
            if (k[0] != "*"):
                context.push_var(k, kwargs[k])
            else:
                mmlwarning("invalid syntax for set")


class Cmd_pop(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        for k in filter(lambda x: x[0][:2] != "__", kwargs.items()):
            context.pop_var(k[1])
        return u""


class Cmd_reset(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        for k in filter(lambda x: x[0][:2] != "__", kwargs.items()):
            context.reset_var(k[1])
        return u""


class Cmd_strlen(MmlKeyword):
    @staticmethod
    def process(context, string, **kwargs):
        mmlwarning("strlen is deprecated use len instead", context)
        return len(string)


class Cmd_len(MmlKeyword):
    @staticmethod
    def process(context, string, **kwargs):
        return len(string)


class Cmd_eq(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga == argb


class Cmd_neq(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga != argb


class Cmd_gt(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga > argb


class Cmd_lt(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga < argb


class Cmd_gte(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga >= argb


class Cmd_lte(MmlKeyword):
    @staticmethod
    def process(context, arga, argb, **kwargs):
        return arga <= argb


class Cmd_getid(MmlKeyword):
    idcntr = 0

    @classmethod
    def process(cls, context, **kwargs):
        tid = cls.idcntr
        cls.idcntr += 1
        return tid


class Cmd_random(MmlKeyword):
    idcntr = 0

    @staticmethod
    def process(context, maxv, **kwargs):
        import random
        maxv = int(maxv)
        return random.randint(0, maxv)


class Cmd_plus(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        kwargsvalues = map(lambda x: x[1], filter(lambda x: x[0][:2] != "__", kwargs.items()))
        argsl = filter(lambda s: type(s) in [float, int] or len(s) != 0, kwargsvalues)
        context.output(unicode(sum(map(float, argsl))))


class Cmd_minus(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        kwargsitems = kwargs.items()
        kwargsitems.sort(key=lambda x: x[0])
        kwargsvalues = map(lambda x: x[1], filter(lambda x: x[0][:2] != "__", kwargsitems))
        argsl = filter(lambda s: isinstance(s, float) or len(s) != 0, kwargsvalues)
        context.output(unicode(reduce(lambda x, y: x - y, map(float, argsl[1:]), float(argsl[0]))))


class Cmd_div(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        kwargsvalues = map(lambda x: x[1], filter(lambda x: x[0][:2] != "__", kwargs.items()))
        argsl = filter(lambda s: isinstance(s, float) or len(s) != 0, kwargsvalues)
        return reduce(lambda x, y: x / y, map(float, argsl[1:]), float(argsl[0]))


class Cmd_mult(MmlKeyword):
    @staticmethod
    def process(args, **kwargs):
        kwargsvalues = map(lambda x: x[1], filter(lambda x: x[0][:2] != "__", kwargs.items()))
        argsl = filter(lambda s: isinstance(s, float) or len(s) != 0, kwargsvalues)
        return reduce(lambda x, y: x * y, map(float, argsl[1:]), float(argsl[0]))


class Cmd_if(MmlKeyword):
    protect_until = [u"else", u"end_if", u"/if"]

    @staticmethod
    def process(context, cond=False, **kwargs):
        #sys.stderr.write("IF")
        context.push_output(OutputBuf())
        context.push_call(("IF", cond, None))


class Cmd_else(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        topstack = context.pop_call()  # ("IF",cond, None))
        buf1 = context.pop_output()
        if (topstack[0] != "IF"):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "IF"), context)
        topstack = (topstack[0], topstack[1], buf1)
        no = OutputBuf()
        context.push_output(no)
        context.push_call(topstack)


class Cmd_end_if(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        try:
            if (context.protect_until):
                context.protect_until = None
            topstack = context.pop_call()  # ("IF",cond, None))
            if (topstack[0] != "IF"):
                mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "IF"), context)
            lbuf = context.pop_output()
            if (reinterpret_on_if):
                do_output = lambda x: context.push_interpret(x, "if")
            else:
                do_output = context.output
            if (topstack[2]):
                if (topstack[1]) and ((topstack[1]) not in [u'0', u"False"]):
                    do_output(topstack[2].buf)
                else:
                    do_output(lbuf.buf)
            else:
                if (topstack[1]) and ((topstack[1]) not in [u'0', u"False"]):
                    do_output(lbuf.buf)
            #sys.stderr.write("/IF\n" + str( lbuf.buf))
        except Exception as e:
            mmlerror("found ?" + str(e), context)


class Cmd_divert(MmlKeyword):
    protect_until = [u"end_divert", u"/divert"]

    @staticmethod
    def process(context, varname, **kwargs):
        if (varname != u"NULL"):
            context.push_output(OutputBuf())
        else:
            context.push_output(NullOutputBuf())
        context.divert_stack.append(varname)
        #context.push_call(("DIVERT",varname)) ### DIVERT ARE NOT PART OF CALLSTACK


class Cmd_end_divert(MmlKeyword):
    @staticmethod
    def process(context, **kwargs):
        if (context.protect_until):
            context.protect_until = None
        #topstack=context.pop_call()
        #assert(topstack[0]=="DIVERT")
        #sys.stderr.write("/divert" +str(context.readers)+"\n") ### DIVERT ARE NOT PART OF CALLSTACK
        bo = context.pop_output()
        #sys.stderr.write("/divert"+str(topstack)+"#" +str(context.readers)+","+str(context.outputs)+"\n")
        #bo=OutputBuf()
        try:
            context.append_var(context.divert_stack.pop(-1), bo.get())
        except Exception as e:
            mmlerror("Does your divert match a divert / Divert stack seems empty ?" + str(e), context)
        #sys.stderr.write("/divert ok\n")


class Cmd_tr(MmlKeyword):
    protect_until = [u"end_tr", u"/tr"]

    @staticmethod
    def process(context, trto2, trfrom1, **kwargs):
        trdelete3 = None
        context.push_output(OutputBuf())
        context.push_call(("TR", trfrom1, trto2, trdelete3))


class Cmd_end_tr(MmlKeyword):
    @staticmethod
    def read_tr_string(s):
        x = 0
        ls = len(s)
        try:
            while True:
                char = s[x]
                if (char == u"\\"):
                    x += 1
                    char = s[x]
                    try:
                        char = {u"n": u"\n", u"t": u"\t"}[char]
                    except KeyError:
                        pass
                if (x <= (ls - 2)) and s[x + 1] == "-":
                    x += 2
                    ed = s[x]
                    if (ed == u"\\"):
                        x += 1
                        ed = s[x]
                        try:
                            ed = {u"n": u"\n", u"t": u"\t"}[ed]
                        except KeyError:
                            pass
                    for i in range(ord(char), ord(ed)):
                        yield i
                    x += 1
                else:
                    yield ord(char)
                    x += 1
        except IndexError:
            raise StopIteration

    @staticmethod
    def process(context, **kwargs):
        if (context.protect_until):
            context.protect_until = None
        topstack = context.pop_call()
        if (topstack[0] != "TR"):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "TR"), context)
        bo = context.pop_output()
        tfrom = topstack[1]
        tto = topstack[2]
        tdelete = topstack[3]
        #sys.stderr.write("tfrom"+str(tfrom))
        ttable = dict(zip(Cmd_end_tr.read_tr_string(tfrom), Cmd_end_tr.read_tr_string(tto)))

        context.output(bo.get().translate(ttable))


class Cmd_not(MmlKeyword):
    @staticmethod
    def process(context, value=False, **kwargs):
        return (not (value)) or (value in [u"0", u"False"])


class Cmd_and(MmlKeyword):
    @staticmethod
    def process(context, value1=False, value2=False, **kwargs):
        return ((value1) and not (value1 in [u"0", u"False"])) and ((value2) and not (value2 in [u"0", u"False"]))


class Cmd_or(MmlKeyword):
    @staticmethod
    def process(context, value1=False, value2=False, **kwargs):
        return ((value1) and not (value1 in [u"0", u"False"])) or ((value2) and not (value2 in [u"0", u"False"]))


class Cmd_int(MmlKeyword):
    @staticmethod
    def process(context, value=0, **kwargs):
        return int(float(value))


class Cmd_for(MmlKeyword):
    protect_until = [u"end_for", u"/for"]

    @staticmethod
    def process(context, varname, listelem, **kwargs):
        context.push_output(OutputBuf())
        context.push_call(("FOR", varname, listelem))


class Cmd_end_for(MmlKeyword):
    @staticmethod
    def process(context, ** kwargs):
        if (context.protect_until):
            context.protect_until = None
        topstack = context.pop_call()
        if (topstack[0] != "FOR"):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "FOR"), context)
        lbuf = context.pop_output()
        for x in (topstack[1]).split(' '):
            context.output(lbuf.buf)


class Cmd_include(MmlKeyword):
    @staticmethod
    def process(context, filename, preserve_subst=False, **kwargs):
        try:
            try:
                bufr = file(filename, "r").read().decode('utf8')
                if (kwargs["__macroblock__"].protected):
                    context.output(bufr)
                else:
                    if (not preserve_subst):
                        old_subst = context.with_relative
                        context.with_relative = False
                    context.push_interpret(bufr, filename)
                    if (not preserve_subst):
                        context.with_relative = old_subst
            except IOError as e:
                mmlwarning("Failed to include '" + filename + "':" + str(e), context, with_stack=True)
        except Exception as e:
            mmlwarning("Error during inlude" + str(e), context)


class Cmd_einclude(MmlKeyword):
    @staticmethod
    def process(context, filename, preserve_subst=False, **kwargs):
        if (not preserve_subst):
            old_subst = context.with_relative
            context.with_relative = False
        context.einclude(filename)
        if (not preserve_subst):
            context.with_relative = old_subst


class Cmd_mml(MmlKeyword):
    @staticmethod
    def process(context, bufr, with_relative="", **kwargs):
        #print "GO MML", bufr, with_relative
        prerel = context.with_relative
        if len(with_relative):
            context.with_relative = bool(int(with_relative))
        context.push_interpret(bufr, "<mml tag>")
        context.with_relative = prerel
# %[set a="%+[b]"]%[def c]%+[mml &a]%[/def]%[c]
# %[set a="%+[b]"]%[def M x]%+[x]%[/def]%[def c]%+[M &a]%[/def]%[c]
# %[set a="%+[b]"]%[def M x]%+[x]%[/def]%[def c]%+[M a]%[/def]%[c]


class Cmd_outfile(MmlKeyword):
    @staticmethod
    def process(context, filename, buffer=None, **kwargs):
        if buffer is None:
            mmlerror("No buffer passed to outfile keyword.", context)
        file(filename, "wb").write(buffer.encode('utf8'))


class Cmd_pipe(MmlKeyword):
    @staticmethod
    def process(context, command, buffer, stripped=False, cwd=None, stderr=None, **kwargs):
        import subprocess
        pc = subprocess.Popen(command.encode('utf8'), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=cwd)
        out, err = pc.communicate(buffer.encode('utf8'))
        if (stderr is None):
            sys.stderr.write(err)
        else:
            out += err
        if (stripped):
            context.output(out.decode('utf8').strip())
        else:
            context.output(out.decode('utf8'))


class Cmd_python(MmlKeyword):
    protect_until = [u"end_python", u"/python"]

    @staticmethod
    def process(context, on_error=None, **kwargs):
        context.push_output(OutputBuf())
        context.push_call(("PYTHON", on_error or "warn"))


class Cmd_end_python(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        if (context.protect_until):
            context.protect_until = None
        topstack = context.pop_call()
        if (topstack[0] != "PYTHON"):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "PYTHON"), context)
        lbuf = context.pop_output()
        sys.stderr.write(str(filter(lambda i: (len(i[1]) == 0), context.symbols.items())))
        dictctx = dict(filter(lambda i: not hasattr(i[1][-1], "call"), context.symbols.items()))
        dictctx["context"] = context
        try:
            exec textwrap.dedent(lbuf.get()) in dictctx
        except Exception as e:
            r = u""
            lno = 0
            for ln in lbuf.get().split(u'\n'):
                r += u"%04d: %s\n" % (lno, ln)
                lno += 1
            sys.stderr.write(r.encode('utf8') + "\n")
            #sys.stderr.write( unicode(dictctx).encode('utf8')+"\n")
            if (topstack[1] == "exit"):
                sys.exit(-1)
            elif (topstack[1] == "ignore"):
                pass
            elif (topstack[1] == "wait_user"):
                if (hasattr(sys, "last_traceback")):
                    traceback.print_tb(sys.last_traceback)
                else:
                    traceback.print_tb(sys.exc_info()[2])
                mmlwarning("python exec failed !" + str(e) + "\nPress enter to continue\n", context)
                sys.stdin.readline()
            else:  # default is warn
                mmlwarning("python exec failed !" + str(e), context)


class Cmd_translate(MmlKeyword):
    protect_until = [u"end_translate", u"/translate"]

    @staticmethod
    def process(context, from_language, to_language, **kwargs):
        context.push_output(OutputBuf())
        context.push_call(("TRANSLATE", from_language, to_language))


class Cmd_end_translate(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        try:
            if (context.protect_until):
                context.protect_until = None
            topstack = context.pop_call()
            if(topstack[0] != "TRANSLATE"):
                mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "TRANSLATE"), context)
            lbuf = context.pop_output()
            import translation
            import translation.Languages as tlanguage
            import translation.Communication as tcom
            t = tlanguage.Translation(tlanguage.Language(topstack[1]), tlanguage.Language(topstack[2]))
            context.output(tcom.translate(lbuf.get(), t))
        except Exception as e:
            mmlwarning("Translation  failed !" + str(e), context)


class Cmd_use(MmlKeyword):
    @staticmethod
    def process(context, env, **kwargs):
        context.open_env(env)


class Cmd_end_use(MmlKeyword):
    @staticmethod
    def process(context, env, **kwargs):
        context.close_env(env)


def sortedlist(l):
    l.sort()
    return l


class Cmd_dump_context(MmlKeyword):
    @staticmethod
    def process(context, with_opened_envs=True, with_symbols=True, with_macros=True, do_exit=True, width=80, **kwargs):
        sys.stderr.write("###################################################### \n")
        sys.stderr.write("MML DUMP CONTEXT \n")
        sys.stderr.write("###################################################### \n")
        if (with_opened_envs):
            sys.stderr.write("Opented Environements :\n")
            sys.stderr.write(unicode(context.opened_env).encode('utf8'))
        if (with_symbols):
            sys.stderr.write("Symbols :\n")
            for k in sortedlist(context.symbols.keys()):
                for l in context.symbols[k]:
                    try:
                        sys.stderr.write(" (%s) " % (', '.join(context.symbols[k].args),))
                    except:
                        pass
                    try:
                        if isinstance(l, unicode):
                            sys.stderr.write(" - %s : %s\n" % (k, l[:width].encode('utf8')))
                        else:
                            sys.stderr.write(" - %s : %s\n" % (k, unicode(l)[:width].encode('utf8')))
                    except:
                        sys.stderr.write(" - %s : ***PRINT ERROR***\n" % (k,))
            sys.stderr.write("###################################################### \n")
        if (do_exit):
            mmlerror("dump context is a debug command... output is stopped", context)


class Cmd_transductor(MmlKeyword):
    protect_until = [u"end_transductor", u"/transductor"]

    @staticmethod
    def process(context, tabledef, compiled=False, maxread=512, cached=False, **kwargs):
        context.push_output(OutputBuf())
        context.push_call(("TRANSDUCTOR", tabledef, compiled, maxread, cached))


class Cmd_end_transductor(MmlKeyword):
    @classmethod
    def call(cls, args, context, mb):
        if (context.protect_until):
            context.protect_until = None
        topstack = context.pop_call()
        if (topstack[0] != "TRANSDUCTOR"):
            mmlerror("Callstack does not match '%s'!='%s'" % (topstack[0], "TRANSDUCTOR"), context)
        lbuf = context.pop_output()
        try:
            if (isinstance(topstack[1], dict)):
                tdef = topstack[1]
            else:
                if topstack[1][0] == u"#":
                    #print context.symbols[topstack[1][1:]]
                    tdef = context.symbols[topstack[1][1:]][-1]
                else:
                    #print "#",topstack[1][1:]
                    tdef = eval(topstack[1])
        except Exception as e:
            mmlwarning("Invalid transductor - could not parse -" + str(e), context)
        try:
            if (topstack[2] and (topstack[2] not in [u"False", u"0"])):
                context.output(CompiledTransductor(tdef).process(lbuf.get()))
            else:
                maxread = topstack[3]
                cached = topstack[4]
                if cached:
                    ct = CachedTransductor(tdef)
                    assert(ct is not None)
                    #print "ct",type(ct),ct
                    r = ct.process(lbuf.get(), maxread=maxread)
                    context.output(r)
                    #sys.exit(0)
                else:
                    context.output(Transductor(tdef).process(lbuf.get(), maxread=maxread))
        except Exception as e:
            mmlwarning("Error during transduction" + repr(e), context)
            sys.stdin.readline()
