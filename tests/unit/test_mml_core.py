from mml.__main__ import mmli


def test_mmli_neutral_on_normal_text():
    assert(mmli("abcd 1234").strip() == "abcd 1234")


def test_macro_def1():
    TXT = """
    %[def a]magic%[/def]%[a]%[a]
    """
    assert(mmli(TXT).strip() == "magicmagic")


def test_macro_def2():
    TXT = """
    %[def a arg1]magic%[/def]%[a]%[a]
    """
    assert(mmli(TXT).strip() == "magicmagic")


def test_macro_def3():
    TXT = """
    %[def a arg1]magic%+[arg1]%+[arg1]%[/def]%[a "1"]%[a]
    """
    assert(mmli(TXT).strip() == "magic11magic")


def test_macro_def_env1():
    TXT = """
    %[def_env a arg1][A]%+[body][/A]%[/def_env]%[a]hello world%[/a]
    """
    assert(mmli(TXT).strip() == "[A]hello world[/A]")


def test_macro_def_env2():
    TXT = """
    %[def b]C%[/def]%[def_env a arg1]%[def b]B%[/def][A]%+[body][/A]%[/def_env]%[b]%[a]%[b]hello world%[b]%[/a]%[b]
    """
    assert(mmli(TXT).strip() == "C[A]Bhello worldB[/A]C")


def test_python1():
    TXT = """
    %[set b="magic"]
    %[python]
    context.output(b[-1].upper())
    %[/python]
    """
    assert(mmli(TXT).strip() == "MAGIC")


def test_transductor1():
    TXT = """
    %[divert TRANS]
    {'a':[
        (u"http://([a-z\-.]+)wide.io",None,None,'re.I'),
        (u"wide.io",u"WIDE IO",None,'re.I')
      ]
    }
    %[/divert]

    %[transductor &TRANS]
    wide.io http://www.wide.io Wide.io
    %[/transductor]
    """
    assert(mmli(TXT).strip() == "WIDE IO http://www.wide.io WIDE IO")
