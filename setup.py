# -*- coding: utf-8 -*-
import os

from setuptools import Extension, find_packages, setup

from Cython.Distutils import build_ext

ext0 = Extension("mml_engine", ["mml/mml_engine.pyx"], extra_compile_args=["-DNDEBUG", "-pthread", "-fPIC", "-fno-strict-aliasing", "-fomit-frame-pointer", "-O6", "-mmmx"])

# DO NOT USE SCRIPT IN THE TWO FOLLOWING DEFINITIONS AS THESE LINES ARE REUSED IN OTHER LANGUAGES
module = "mml"
VERSION_FILE = "mml/VERSION"

version = None
try:
    with open(os.path.join(os.path.dirname(__file__), VERSION_FILE), 'r') as fd:
        version = fd.read()
except:
    pass

if not version:
    version = "0.0.0"

setup(
    name=module,
    version=version,
    description='The Meta Macro Language',
    author='Bertrand Nouvel',
    author_email='support@wide.io',
    url='https://gitlab.pet.wide.io/oss/mml',
    packages=['mml'],
    cmdclass={'build_ext': build_ext},
    ext_modules=[ext0],
    license='BSD',
    classifiers=(
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython',
    ),
    install_requires=["colorterm"],
    include_package_data=True,
    package_dir={module: module},
    package_data={
        '': ['LICENSE'],
        module: ['VERSION']
    },
    entry_points={
        'console_scripts': [
            'mml = mml:__main__'
        ],
    }
)
